# Version information used on all builds
PRODUCT_BUILD_PROP_OVERRIDES += BUILD_VERSION_TAGS=release-keys USER=android-build BUILD_UTC_DATE=$(shell date +"%s")

# XeroX Rom Version
XEROX_VERSION_MAJOR = 1
XEROX_VERSION_MINOR = 0

PRODUCT_PROPERTY_OVERRIDES += \
    ro.xerox.version=XeroX-AOSP-$(XEROX_VERSION_MAJOR).$(XEROX_VERSION_MINOR)-$(TARGET_PRODUCT)
