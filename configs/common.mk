# Common overlay
PRODUCT_PACKAGE_OVERLAYS += vendor/xerox/overlay/common

# Common dictionaries
PRODUCT_PACKAGE_OVERLAYS += vendor/xerox/overlay/dictionaries

# Common product property overrides
PRODUCT_PROPERTIES_OVERRIDE += \
        keyguard.no_require_sim=true \
        ro.url.legal=http://www.google.com/intl/%s/mobile/android/basic/phone-legal.html \
        ro.com.google.clientidbase=android-google \
        ro.com.android.wifi-watchlist=GoogleGuest \
        ro.setupwizard.enterprise_mode=1 \
        ro.com.android.dateformat=MM-dd-yyyy \
        ro.com.android.dataroaming=false \
        persist.sys.root_access=1

# init.d support
PRODUCT_COPY_FILES += \
    vendor/xerox/prebuilt/common/etc/init.d/00banner:system/etc/init.d/00banner \
    vendor/xerox/prebuilt/common/bin/sysinit:system/bin/sysinit

# userinit support
PRODUCT_COPY_FILES += \
    vendor/xerox/prebuilt/common/etc/init.d/90userinit:system/etc/init.d/90userinit

# XeroX AOSP init file
PRODUCT_COPY_FILES += \
    vendor/xerox/prebuilt/common/etc/init.local.rc:root/init.xerox.rc

# Enable SIP+VoIP
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml

# block stock OTAs
PRODUCT_COPY_FILES += \
    vendor/xerox/prebuilt/common/bin/otablock:system/bin/otablock

# enable ADB authentication if not on eng build
ifneq ($(TARGET_BUILD_VARIANT),eng)
ADDITIONAL_DEFAULT_PROPERTIES += ro.adb.secure=1
endif

# Optional packages
PRODUCT_PACKAGES += \
    Basic

# Extra tools
PRODUCT_PACKAGES += \
    openvpn \
    e2fsck \
    mke2fs \
    tune2fs \
    bash \
    nano \
    mount.exfat \
    fsck.exfat \
    mkfs.exfat \
    ntfsfix \
    ntfs-3g

# Openssh
PRODUCT_PACKAGES += \
    scp \
    sftp \
    ssh \
    sshd \
    sshd_config \
    ssh-keygen \
    start-ssh

# rsync
PRODUCT_PACKAGES += \
    rsync

# SELinux filesystem labels
PRODUCT_COPY_FILES += \
     vendor/xerox/prebuilt/common/etc/init.d/50selinuxrelabel:system/etc/init.d/50selinuxrelabel

-include vendor/xerox/sepolicy/sepolicy.mk

# Bring in all video prebuilts
$(call inherit-product, frameworks/base/data/videos/VideoPackage2.mk)

# Inherit common product build prop overrides
-include vendor/xerox/configs/common_versions.mk